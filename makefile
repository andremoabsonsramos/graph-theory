SRCDIR = src
BINDIR = bin
OBJDIR = obj

CC = g++

CCFLAGS = 

COMPILE = $(CC) $(SRCDIR)/$@.cpp -o $(BINDIR)/$@ $(CCFLAGS)

all: graph.o graphalgorithm.o main

graphalgorithm.o: $(SRCDIR)/graphalgorithm.h $(SRCDIR)/graphalgorithm.cpp
	$(CC) -c $(SRCDIR)/graphalgorithm.cpp -o $(OBJDIR)/graphalgorithm.o

graph.o: $(SRCDIR)/graph.h $(SRCDIR)/graph.cpp
	$(CC) -c $(SRCDIR)/graph.cpp -o $(OBJDIR)/graph.o

main: $(SRCDIR)/main.cpp
	$(CC) $(SRCDIR)/main.cpp $(OBJDIR)/graph.o $(OBJDIR)/graphalgorithm.o $(CCFLAGS) -o $(BINDIR)/main

.PHONY: clean
clean:
	rm -f $(BINDIR)/*
	rm -f $(OBJDIR)/*



