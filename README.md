# graph-theory

# Compilando e executando 

1. make
2. ./bin/main "algorithm name" "input file"

algorithm name = dijkstra, bellman_ford, floyd_warshall or prim

# Exemplo de execução

./bin/main prim data/graph_prim.txt
