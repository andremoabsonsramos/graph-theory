#include <iostream>
#include <limits>
#include <queue>
#include <string>
#include "graph.h"
#include "graphalgorithm.h"

using std::cout;
using std::cerr;
using std::endl;
using std::exception;
using std::string;

int main(int argc, char const *argv[])
{
    try
    {
        string algorithm = argv[1];
        string file = argv[2];

        Graph* g = Graph::from_file(file);

        if (algorithm == "dijkstra") {
            cout << "dijsktra: " << endl;
            graphalgorithm::dijkstra(g, 0);

        } else if (algorithm == "bellman_ford") {
            cout << "bellman ford: " << endl;
            graphalgorithm::bellman_ford(g, 0);

        } else if (algorithm == "floyd_warshall") {
            cout << "floyd_warshall: " << endl;
            graphalgorithm::floyd_warshall(g);

        } else if (algorithm == "prim") {
            cout << "prim: " << endl;
            if (g->is_connected())
                graphalgorithm::prim(g, 0);
            else
                cout << "graph is not connected" << endl;
        }

        delete g;
    }
    catch(exception& e)
    {
        cerr << e.what() << endl;
    }

    return 0;
}
