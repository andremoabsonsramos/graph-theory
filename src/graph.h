#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <memory>
#include <utility>
#include <vector>
#include <ostream>
#include <stdexcept>
#include <string>
#include <fstream>
#include <string.h>

using std::cout;
using std::cin;
using std::endl;
using std::ostream;
using std::vector;
using std::pair;
using std::out_of_range;
using std::string;
using std::ifstream;
using std::invalid_argument;
using std::shared_ptr;
using std::make_shared;

enum GraphType {
    DIRECTED,
    UNDIRECTED
};

struct Edge {
    int from;
    int to;
    double weight;

    Edge() {}
    Edge(int u, int v) : from(u), to(v) {}
    Edge(int u, int v, double w) : from(u), to(v), weight(w) {}

    friend ostream& operator<< (ostream& os, Edge edge) {
        os << "(" << edge.from << " -> " << edge.to << ", " << edge.weight << ")";
    }

    friend ostream& operator<< (ostream& os, Edge* edge) {
        os << "(" << edge->from << " -> " << edge->to << ", " << edge->weight << ")";
    }
};

class Graph
{
private:
    GraphType type;
    vector<Edge*> edges;
    vector<vector<Edge*>> adjlist;
    vector<vector<Edge*>> adjmatrix;

public:
    int n_vertex = 0;
    int n_edge = 0;

    Graph(int n_vertex);
    static Graph* from_file(string filename);
    void add_edge(Edge* edge);
    Edge* get_edge(int from, int to);
    bool is_connected();
    void dfs(int source, bool visited[]);
    vector<Edge*>& get_edges();
    vector<vector<Edge*>>& get_adjlist();
    vector<Edge*>& get_adjlist(int vertex);
    vector<vector<Edge*>>& get_adjmatrix();
    void set_weight(Edge* edge, double weight);
    ~Graph();
};

#endif