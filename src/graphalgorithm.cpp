#include "graphalgorithm.h"

namespace graphalgorithm {
    bool debug = false;

    void dijkstra(Graph* g, int source) 
    {
        vector<bool> visited(g->n_vertex);
        vector<double> dist(g->n_vertex);
        vector<int> pred(g->n_vertex);
        auto comparator = [&](const int& u, const int& v)
        {
            return dist[u] > dist[v];
        };
        priority_queue<int, vector<int>, decltype(comparator)> queue(comparator);

        for (int v = 0; v < g->n_vertex; ++v)
        {
            dist[v] = numeric_limits<double>::max();
            pred[v] = -1;
            visited[v] = false;
        }

        dist[source] = 0;
        
        queue.push(source);

        // dijkstra algorithm relax only selected vertex from priority queue
        while (!queue.empty()) 
        {
            int from = queue.top(); queue.pop();

            if (debug) cout << from << " " << dist[from] << endl;

            for (const Edge* edge : g->get_adjlist(from)) 
            {
                if (!visited[edge->to])
                {
                    double mindist = dist[edge->to];
                    double newdist = dist[edge->from] + edge->weight;

                    if (mindist > newdist) // check if distance to next vertex is lower then min dist 
                    {
                        if (debug) cout << mindist << " > " << newdist << endl;

                        // if is lower update min dist
                        dist[edge->to] = newdist;
                        pred[edge->to] = edge->from;
                    }

                    queue.push(edge->to);
                }
            }

            visited[from] = true;
        }

        cout << "min dist array: ";
        print<double>(dist);
        cout << "predecessor array: ";
        print<int>(pred);
    }

    void bellman_ford(Graph* g, int source)
    {
        vector<bool> visited(g->n_vertex);
        vector<double> dist(g->n_vertex);
        vector<int> pred(g->n_vertex);

        for (int v = 0; v < g->n_vertex; ++v)
        {
            dist[v] = numeric_limits<double>::max();
            pred[v] = -1;
            visited[v] = false;
        }

        dist[source] = 0;
        
        // bellman ford algorithm relax all vertex
        for (int from = 0; from < g->n_vertex; ++from)
        {
            for (const Edge* edge : g->get_adjlist(from)) 
            {
                if (!visited[edge->to])
                {
                    double mindist = dist[edge->to];
                    double newdist = dist[edge->from] + edge->weight;

                    if (mindist > newdist) // check if distance to next vertex is lower then min dist 
                    {
                        if (debug) cout << mindist << " > " << newdist << endl;

                        // if is lower update min dist
                        dist[edge->to] = newdist;
                        pred[edge->to] = edge->from;
                    }
                }
            }

            visited[from] = true;
        }

        for (const Edge* edge : g->get_edges()) {
            if (dist[edge->from] != numeric_limits<double>::max() && 
                dist[edge->to] > dist[edge->from] + edge->weight)
            {
                cout << "graph contains a negative weight cycle" << endl;
                return;
            } 
        }

        cout << "min dist array: ";
        print<double>(dist);
        cout << "predecessor array: ";
        print<int>(pred);
    }

    void prim(Graph* g, int source)
    {
        vector<bool> visited(g->n_vertex);
        vector<double> minweight(g->n_vertex);
        vector<int> pred(g->n_vertex);
        auto comparator = [&](const int& u, const int& v)
        {
            return minweight[u] > minweight[v];
        };
        priority_queue<int, vector<int>, decltype(comparator)> queue(comparator);

        for (int v = 0; v < g->n_vertex; ++v)
        {
            minweight[v] = numeric_limits<double>::max();
            pred[v] = -1;
            visited[v] = false;
        }

        minweight[source] = 0;
        
        queue.push(source);

        // prim
        while (!queue.empty()) 
        {
            int from = queue.top(); queue.pop();

            if (debug) cout << from << " " << minweight[from] << endl;

            for (const Edge* edge : g->get_adjlist(from)) 
            {
                if (!visited[edge->to])
                {  
                    // minweight(to) = min(minweight(to), cost(from, to))
                    if (edge->weight < minweight[edge->to])
                    {
                        minweight[edge->to] = edge->weight;
                        pred[edge->to] = edge->from;
                    }

                    queue.push(edge->to);
                }
            }
            visited[from] = true;
        }

        cout << "min minweight array: ";
        print<double>(minweight);
        cout << "predecessor array: ";
        print<int>(pred);
        double sum = 0;
        for (const double v : minweight) sum += v;
        cout << "sum edges weights: " << sum << endl;
    }

    void floyd_warshall(Graph* g)
    {
        double dist[g->n_vertex][g->n_vertex];
        int pred[g->n_vertex][g->n_vertex];

        for (int i = 0; i < g->n_vertex; ++i)
        {
            for (int j = 0; j < g->n_vertex; ++j)
            {
                const Edge* edge = g->get_adjmatrix()[i][j];

                if (edge != nullptr)
                {
                    dist[i][j] = edge->weight;
                    pred[i][j] = edge->from;
                }
                else
                {
                    if (i == j)
                        dist[i][j] = 0;
                    else 
                        dist[i][j] = numeric_limits<double>::max();
                    
                    pred[i][j] = -1;
                }
            }
        }

        for (int k = 0; k < g->n_vertex; ++k)
        {
            for (int i = 0; i < g->n_vertex; ++i)
            {
                for (int j = 0; j < g->n_vertex; ++j)
                {
                    if (dist[i][j] > dist[i][k] + dist[k][j])
                    {
                        dist[i][j] = dist[i][k] + dist[k][j];
                        pred[i][j] = pred[k][j];
                    }
                }
            }
        }

        cout << "min dist matrix: " << endl;
        for (int i = 0; i < g->n_vertex; ++i)
        {
            for (int j = 0; j < g->n_vertex; ++j)
                cout << dist[i][j] << " ";
            cout << endl;
        }
        cout << "predecessor matrix: " << endl;
        for (int i = 0; i < g->n_vertex; ++i)
        {
            for (int j = 0; j < g->n_vertex; ++j)
                cout << pred[i][j] << " ";
            cout << endl;
        }
    }
}