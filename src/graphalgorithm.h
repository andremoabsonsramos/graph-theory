#ifndef GRAPHALGORITHM_H
#define GRAPHALGORITHM_H

#include <iostream>
#include <limits>
#include <queue>
#include "graph.h"

using std::cout;
using std::endl;
using std::priority_queue;
using std::numeric_limits;

namespace graphalgorithm {
    extern bool debug;

    template <typename T>
    void print(vector<T> collection) 
    {
        cout << "[ ";
        int i = 0;
        int last = collection.size() - 1;
        for (auto value : collection) 
            cout << i++ << ": " << value << ((i < last) ? ", " : "");
        cout << " ]" << endl;
    }

    void dijkstra(Graph* g, int source);
    void bellman_ford(Graph* g, int source);
    void floyd_warshall(Graph* g); 
    void prim(Graph* g, int source);
}


#endif