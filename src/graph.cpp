#include "graph.h"

Graph::Graph(int n_vertex)
{
    this->n_vertex = n_vertex;
    // this->type = type;
    this->adjlist.resize(n_vertex);
    this->adjmatrix.resize(n_vertex);
    for (int i = 0; i < n_vertex; ++i)
        this->adjmatrix[i].resize(n_vertex);
}

Graph* Graph::from_file(string filename) {
    ifstream file(filename);

    if (!file.is_open())
        throw invalid_argument("file not found");

    int n_vertex, n_edge;

    file >> n_vertex >> n_edge;

    Graph* g = new Graph(n_vertex);

    for (int i = 0; i < n_edge; ++i)
    {
        int from, to;
        double weight;

        file >> from >> to >> weight;

        g->add_edge(new Edge(from, to, weight));
    }

    return g;
}

void Graph::add_edge(Edge* edge)
{
    if (edge->from >= 0 && edge->to >= 0 && edge->from < this->n_vertex && edge->to < this->n_vertex)
    {
        this->edges.push_back(edge);
        this->adjlist[edge->from].push_back(edge);
        this->adjmatrix[edge->from][edge->to] = edge;
        this->n_edge++;
    }
    else 
    {
        throw out_of_range("vertex value out of range");
    }
}
bool Graph::is_connected() {
    bool visited[this->n_vertex];
    memset(visited, 0, sizeof visited);

    dfs(0, visited);

    for (int v = 0; v < this->n_vertex; ++v) 
        if (!visited[v])
            return false;

    return true;
}

void Graph::dfs(int source, bool visited[]) 
{
    visited[source] = true;

    for (const Edge* edge : this->get_adjlist(source))
    {
        if (!visited[edge->to])
        {
            dfs(edge->to, visited);
        }
    }
}


Edge* Graph::get_edge(int from, int to)
{
    if (from >= 0 && to >= 0 && from < this->n_vertex && to < this->n_vertex)
    {
        return this->adjmatrix[from][to];
    }
    else 
    {
        throw out_of_range("vertex value out of range");
    }
}

vector<Edge*>& Graph::get_edges() {
    return this->edges;
}

vector<vector<Edge*>>& Graph::get_adjlist()
{
    return this->adjlist;
}

vector<Edge*>& Graph::get_adjlist(int vertex) 
{
    if (vertex >= 0 && vertex < this->n_vertex)
    {
        return this->adjlist[vertex];
    }
    else 
    {
        throw out_of_range("vertex value out of range"); 
    }    
}

vector<vector<Edge*>>& Graph::get_adjmatrix()
{
    return this->adjmatrix;
}

void Graph::set_weight(Edge* edge, double weight)
{
    edge->weight = weight;
}

Graph::~Graph()
{
    cout << "destroyng graph..." << endl;
    for (vector<Edge*> v : this->adjlist)
    {
        for (Edge* edge : v)
        {
            delete edge;
        }
        v.clear();
    }

    this->adjlist.clear();

    for (int i = 0; i < this->n_vertex; ++i)
    {
        for (int j = 0; j < this->n_vertex; ++j)
        {
            delete this->adjmatrix[i][j];
        }
    }
}
